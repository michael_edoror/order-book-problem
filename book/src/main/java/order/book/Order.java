package order.book;

import java.math.BigDecimal;

/**
 * 
 * @author medoror1
 *
 */
public interface Order {

  enum Side {
    BID("B"), ASK("S");

    final String sideType;

    Side(String type) {
      this.sideType = type;
    }

    static Side getSide(String theSide) {
      Side s = null;
      for(Side side: Side.values()){
        if( theSide.equals(side.sideType)) s = side;
      }
      return s;
    }
  }
  enum Type{
    ADD, REMOVE;
  }
  public String getOrder_id();
  
  public int getSize();
  
  public void setSize(int size);
  
  public Side getSide();
  
  public BigDecimal getPrice();
  
  public long getTimestamp();
  
  public Type getType();
}
