package order.book;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import order.book.Order.Side;
import order.book.Order.Type;

/**
 * 
 * @author medoror1
 *
 */
public class Book {
  
  enum State{
    ABLE, NOT_ABLE;
  }

  private int targetSize;
  private BookDetail bids;
  private BookDetail asks;
  private BigDecimal currentPrice = BigDecimal.ZERO;

  private int runningBids = 0;
  private int runningAsks = 0;

  private List<State> bidStates = new ArrayList<>();
  private List<State> askStates = new ArrayList<>();

  /** Formatter for string result */
  private DecimalFormat df = new DecimalFormat("####.00");

  public Book(int targetSize) {
    this.targetSize = targetSize;
    bids = new BookDetail(new TreeSet<Order>((Order o1, Order o2) -> {
      int cmp = o2.getPrice().compareTo(o1.getPrice());
      if (cmp == 0) cmp = Integer.compare(o2.getSize(), o1.getSize());
      return cmp;
    }));
    asks = new BookDetail(new TreeSet<Order>((Order o1, Order o2) -> {
      int cmp = o1.getPrice().compareTo(o2.getPrice());
      if (cmp == 0) cmp = Integer.compare(o1.getSize(), o2.getSize());
      return cmp;
    }));
  }

  /**
   * Adds an order to the book
   * @param order
   * @return String result of the add order
   */
  public String add(Order order) {
    if (order.getSide() == Side.BID) {
      bids.add(order);
      runningBids += order.getSize();
    } else {
      asks.add(order);
      runningAsks += order.getSize();
    }
    return executeAdd(order);
  }

  /**
   * Removes an order from the book
   * @param order
   * @return String result of the remove order
   */
  public String remove(Order order) {
    Order oldOrder;
    if (bids.getOrderMap().containsKey(order.getOrder_id())) {
      oldOrder = bids.getOrderMap().get(order.getOrder_id());
      remove(oldOrder, order, bids);
    } else {
      oldOrder = asks.getOrderMap().get(order.getOrder_id());
      remove(oldOrder, order, asks);
    }

    if(bids.getSizeOfBook() >= targetSize || asks.getSizeOfBook() >= targetSize ){
      return executeAdd(oldOrder); //still some books to be bought
    }
    return executeRemove(order.getTimestamp(), oldOrder);
  }

  /**
   * 
   * @param order
   * @return
   */
  private String executeRemove(long timestamp, Order order) {
    String result = "";
    
    if (order.getSide() == Side.BID && hasTransitioned(bidStates)) {
      result = timestamp + " S " + "NA";
    } else if (order.getSide() == Side.ASK && hasTransitioned(askStates)) {
      result = timestamp + " B " + "NA";
    }
    return result;
  }
  
  /**
   * If the previous state is equal to the current state then there is no transition
   * @param states
   * @return
   */
  private boolean hasTransitioned(List<State> states){
    if(states.isEmpty() || states.size() < 2) return false;
    int currentIndex = states.size() -1;
    return !states.get(currentIndex).equals(states.get(currentIndex - 1));
  }

  /**
   * Removes the order
   * @param oldOrder
   * @param order
   * @param bookDetail
   */
  private void remove(Order oldOrder, Order order, BookDetail bookDetail) {
    if (oldOrder != null) {
      int leftover = oldOrder.getSize() - order.getSize();
      if (leftover <= 0) {
        bookDetail.remove(oldOrder);
      } else {
        bookDetail.remove(oldOrder);
        BaseOrder newOrder = new BaseOrder((BaseOrder) oldOrder);
        newOrder.setSize(leftover);
        bookDetail.add(newOrder);
      }
      if( bids.getSizeOfBook() <= targetSize){
        bidStates.add(State.NOT_ABLE);
      }
      else if( asks.getSizeOfBook() <= targetSize ){
        askStates.add(State.NOT_ABLE);
      }
    }
  }

  /**
   * Executes an add order message
   * @param order
   * @return String formatted result
   */
  private String executeAdd(Order order) {
    if( order.getSide() == Side.BID ) return execute(bids, "S", order, bidStates, runningBids);
    else return execute(asks, "B", order, askStates, runningAsks);
  }
  
  /**
   * Executes an add an ask or bid to the book
   * @param book
   * @param buySell
   * @param order
   * @param states
   * @param runningTotal
   * @return String formatted result
   */
  private String execute(BookDetail book, String buySell, Order order, List<State>states, int runningTotal){
    String result = "";
    if (book.getSizeOfBook() >= targetSize) {
      runningTotal = targetSize;
      currentPrice = BigDecimal.ZERO;
      states.add(State.ABLE);

      for (Iterator<Order> iterator = book.getOrderSet().iterator(); iterator.hasNext();) {
        Order newOrder = iterator.next();
        if (newOrder.getSize() > runningTotal) {
          currentPrice = currentPrice.add(newOrder.getPrice().multiply(new BigDecimal(runningTotal)));
          break;
        } else if (newOrder.getSize() <= runningTotal) {
          runningTotal -= newOrder.getSize();
          currentPrice = currentPrice.add(newOrder.getPrice().multiply(new BigDecimal(newOrder.getSize())));
        }
      }
      result = order.getTimestamp() +" "+buySell+" "+ df.format(currentPrice);
    }
    return result;
  }

  /**
   * Submits an order to the book
   * @param newOrder
   * @return
   */
  public String submit(Order newOrder) {
    if (newOrder.getType() == Type.ADD)
      return add(newOrder);
    else
      return remove(newOrder);
  }
}
