package order.book;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main entry point
 * @author medoror1
 *
 */
public class Pricer {

  private static final Pattern ADD_ORDER_REGEX = Pattern.compile("(\\d+) A (\\w+) ([BS]) (\\d+.\\d+) (\\d+)");
  private static final Pattern REDUCE_ORDER_REGEX = Pattern.compile("(\\d+) R (\\w+) (\\d+)");

  public static void main(String[] args) {
    if( args.length == 0 ){
      System.err.println("Usage: This program takes 1 command line argument in the form of pricer < positive integer>");
      System.exit(-1);
    }
    int targetSize = 0;
    try {
      targetSize = Integer.parseInt(args[0]);
    } catch (NullPointerException | NumberFormatException ne) {
      System.err.println("Usage: This program takes 1 command line argument in the form of pricer < positive integer>");
      System.exit(-1);
    }

    Book book = new Book(targetSize);

    try (Scanner scanner = new Scanner(System.in)) {

      Order newOrder;
      while (true) {
        String line = scanner.nextLine();
        newOrder = processLine(line);
        if (newOrder == null) {
          System.err.println("Line: " + line + " is not formatted properly");
          continue; // continue to the next line
        }
        String output = book.submit(newOrder);
        if( !output.isEmpty() ) System.out.println(output);
      }
    }catch(NoSuchElementException ns){
      // End of input
      // System.err.println("No more lines found");
      System.exit(-1);
    }
  }

  /**
   * Uses regex to read given line and create a formated order
   * @param line
   * @return an Order object or null if line is malformed
   */
  private static Order processLine(String line) {

    Matcher matcher = ADD_ORDER_REGEX.matcher(line);
    if (matcher.matches()) {
      // ADD
      return new BaseOrder(Long.parseLong(matcher.group(1)), matcher.group(2), matcher.group(3), 
          new BigDecimal(matcher.group(4)), Integer.parseInt(matcher.group(5)));
    }
    matcher = REDUCE_ORDER_REGEX.matcher(line);
    if (matcher.matches()) {
      // REMOVE
      return new BaseOrder(Long.parseLong(matcher.group(1)), matcher.group(2), Integer.parseInt(matcher.group(3)));
    }
    return null; // parsing went wrong
  }
}
