package order.book;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author medoror1
 *
 */
public class BookDetail {

  private Map<String, Order> orderMap;
  private Set<Order> orderSet;
  private int sizeOfBook = 0;

  public BookDetail(Set<Order> set) {
    this.orderSet = set;
    this.orderMap = new HashMap<>();
  }

  public void add(Order order) {
    orderMap.put(order.getOrder_id(), order);
    orderSet.add(order);
    sizeOfBook += order.getSize();
  }

  public void remove(Order order) {
    orderMap.remove(order.getOrder_id());
    orderSet.remove(order);
    sizeOfBook -= order.getSize();
  }

  public Map<String, Order> getOrderMap() {
    return orderMap;
  }

  public void setOrderMap(Map<String, Order> orderMap) {
    this.orderMap = orderMap;
  }

  public Set<Order> getOrderSet() {
    return orderSet;
  }

  public void setOrderSet(Set<Order> orderSet) {
    this.orderSet = orderSet;
  }

  public int getSizeOfBook() {
    return sizeOfBook;
  }

  public void setSizeOfBook(int sizeOfBook) {
    this.sizeOfBook = sizeOfBook;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("BookDetail [orderMap=");
    builder.append(orderMap);
    builder.append(", orderSet=");
    builder.append(orderSet);
    builder.append(", sizeOfBook=");
    builder.append(sizeOfBook);
    builder.append("]");
    return builder.toString();
  }
}
