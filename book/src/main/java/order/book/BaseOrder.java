package order.book;

import java.math.BigDecimal;

/**
 * This class represents an add or remove order
 * @author medoror1
 *
 */
public class BaseOrder implements Order {

  private long timestamp;
  private Side side;
  private Type type;
  private String order_id;
  private BigDecimal price;
  private int size;

  public BaseOrder(long timestamp, String id, String side, BigDecimal price, int size) {
    this.timestamp = timestamp;
    this.side = Side.getSide(side);
    this.order_id = id;
    this.price = price;
    this.size = size;
    this.setType(Type.ADD);
  }

  public BaseOrder(long timestamp, String id, int size) {
    this.timestamp = timestamp;
    this.order_id = id;
    this.size = size;
    this.setType(Type.REMOVE);
  }
  
  public BaseOrder(BaseOrder that){
    this.timestamp = that.timestamp;
    this.side = that.side;
    this.type = that.type;
    this.order_id = that.order_id;
    this.price = that.price;
    this.size = that.size;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public Side getSide() {
    return side;
  }


  public void setSide(Side side) {
    this.side = side;
  }

  public String getOrder_id() {
    return order_id;
  }

  public void setOrder_id(String order_id) {
    this.order_id = order_id;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((order_id == null) ? 0 : order_id.hashCode());
    result = prime * result + ((price == null) ? 0 : price.hashCode());
    result = prime * result + ((side == null) ? 0 : side.hashCode());
    result = prime * result + size;
    result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BaseOrder other = (BaseOrder) obj;
    if (order_id == null) {
      if (other.order_id != null)
        return false;
    } else if (!order_id.equals(other.order_id))
      return false;
    if (price == null) {
      if (other.price != null)
        return false;
    } else if (!price.equals(other.price))
      return false;
    if (side != other.side)
      return false;
    if (size != other.size)
      return false;
    if (timestamp != other.timestamp)
      return false;
    if (type != other.type)
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("BaseOrder [timestamp=");
    builder.append(timestamp);
    builder.append(", side=");
    builder.append(side);
    builder.append(", type=");
    builder.append(type);
    builder.append(", order_id=");
    builder.append(order_id);
    builder.append(", price=");
    builder.append(price);
    builder.append(", size=");
    builder.append(size);
    builder.append("]");
    return builder.toString();
  }
}
