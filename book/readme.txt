Compile:
mvn clean package

Running:
# Print results to stdout
java -jar target/book-jar-with-dependencies.jar 200 < inputFiles/small_pricer.in

# Print results to output file
java -jar target/book-jar-with-dependencies.jar 200 < inputFiles/small_pricer.in > outputFiles/small_pricer.out
