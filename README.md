Q. How did you choose your implementation language?
A. Java is the most comfortable language for me and is the language that I have experience with the most.  Java is a very mature and strong enough language to fit most programming use cases including this one

Q. What is the time complexity for processing an Add Order message?
The current implementation uses both a TreeSet and HashMap for ordering and random access.  The add 
order mechansim will add each order to both structures and will traverse the order set at most N times to collect books.  The implementation will be O(log N)

Q. What is the time complexity for processing a Reduce Order message?
A. The current implementation will remove an order message from both the backing TreeSet and HashMap. Removal from a TreeSet being slower in the worst case than the HashMap leaves the implementation of Reduce Order to be O(log N)

Q. If your implementation were put into production and found to be too slow, what ideas would you try out to improve its performance? (Other than reimplementing it in a different language such as C or C++.)
A.  To make this implementation faster I could do the following:
* Removing the use of BigDecimal as it is slow.  I used BigDecimal for its accurate $ representations and ease of format printing. However, using a double instead could be more efficient
 
